import java.lang.annotation.Annotation;

@MyAnnotation(num = 5,name = "Dmytro")
public class Student {
    Student(){
    }
    public void showAnnotationValues(){
      Annotation annotation = this.getClass().getAnnotation(MyAnnotation.class);
      System.out.println("Name: "+((MyAnnotation) annotation).name()
          + "\nNum: " + ((MyAnnotation) annotation).num());
    }
}
